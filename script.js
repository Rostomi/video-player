const video = document.getElementById("video");
const controls = document.getElementById("controls");
const playVid = document.getElementById("play");
const stopVid = document.getElementById("stop");
const seekVid = document.getElementById("seek");
const progress = document.getElementById("progress");
const timestamp = document.getElementById("timestamp");

video.addEventListener("loadedmetadata", function () {
  let videoLengthMin = Math.floor(video.duration / 60);
  if (videoLengthMin < 10) {
    videoLengthMin = "0" + String(videoLengthMin);
  }
  let videoLengthSecs = Math.floor(video.duration % 60);
  if (videoLengthSecs < 10) {
    videoLengthSecs = "0" + String(videoLengthSecs);
  }
  timestamp.innerHTML = `00:00 / ${videoLengthMin}:${videoLengthSecs}`;
});

// play and pause video
function toggleVideoStatus() {
  if (video.paused) {
    video.play();
  } else {
    video.pause();
  }
}

// update play pause icon
function updatePlayIcon() {
  if (video.paused) {
    play.innerHTML = `<i class="fa fa-play fa-2x"></i>`;
  } else {
    play.innerHTML = `<i class="fa fa-pause fa-2x"></i>`;
  }
}

function updateProgress() {
  progress.value = (video.currentTime / video.duration) * 100;

  let mins = Math.floor(video.currentTime / 60);
  if (mins < 10) {
    mins = "0" + String(mins);
  }
  let secs = Math.floor(video.currentTime % 60);
  if (secs < 10) {
    secs = "0" + String(secs);
  }

  let fullMins = Math.floor(video.duration / 60);
  if (fullMins < 10) {
    fullMins = "0" + String(fullMins);
  }
  let fullSecs = Math.floor(video.duration % 60);
  if (fullSecs < 10) {
    fullSecs = "0" + String(fullSecs);
  }

  timestamp.innerHTML = `${mins}:${secs} / ${fullMins}:${fullSecs}`;
}

function setVideoProgress() {
  video.currentTime = Math.floor((progress.value * video.duration) / 100);
}

function stopVideo() {
  video.currentTime = 0;
  video.pause();
}

function seekVideo10Secs() {
  video.currentTime = video.currentTime + 10;
}

video.addEventListener("click", toggleVideoStatus);
video.addEventListener("pause", updatePlayIcon);
video.addEventListener("play", updatePlayIcon);
video.addEventListener("timeupdate", updateProgress);

playVid.addEventListener("click", toggleVideoStatus);
stopVid.addEventListener("click", stopVideo);
seekVid.addEventListener("click", seekVideo10Secs);

progress.addEventListener("change", setVideoProgress);
